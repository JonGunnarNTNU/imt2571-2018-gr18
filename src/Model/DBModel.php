<?php
/** The Model implementation of the IMT2571 Assignment #1 MVC-example, storing data in a MySQL database using PDO.
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */

require_once("AbstractModel.php");
require_once("Book.php");

/** The Model is the class holding data about a collection of books.
 * @todo implement class functionality.
 */
class DBModel extends AbstractModel
{
    protected $db = null;
    
    /**
     * @param PDO $db PDO object for the database; a new one will be created if no PDO object
     *                is passed
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    public function __construct($db = null)
    {
        if ($db) {
            $this->db = $db;
        } else {
            $this->db = new PDO('mysql:host=localhost;dbname=test;charset=utf8','root', '', array(PDO::ATTR_EMULATE_PREPARES => false,PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

        }
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return An array of book objects indexed and ordered by their id.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    public function getBookList()
    {
        $booklist = array();
        $statement = $this->db->query('SELECT * FROM book ORDER BY id');
        while($row = $statement->fetch(PDO::FETCH_ASSOC))
        {
            $book = new Book($row['title'],$row['author'],$row['description'],$row['id']);
            array_push($booklist, $book);
        }
        return $booklist;
    }
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */

    public function getBookById($id)
    {
        $this::verifyID($id);
        $book = null;
        $statement = $this->db-> prepare("SELECT * FROM book WHERE id = :id");
        $statement->bindValue(':id',$id,PDO::PARAM_STR);
        $statement->execute();
        $row = $statement->fetch(PDO::FETCH_ASSOC);
        if($row)
            $book = new Book($row['title'],$row['author'],$row['description'],$row['id']);

        return $book;
    }
    
    /** Adds a new book to the collection.
     * @param Book $book The book to be added - the id of the book will be set after successful insertion.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    
    public function addBook($book) {

        $this::verifyBook($book);
        $statement = $this->db-> prepare("INSERT INTO book(title,author,description) VALUES(:title,:author,:description)");
        $statement-> bindValue(':title', $book->title, PDO::PARAM_STR);
        $statement-> bindValue(':author',$book->author, PDO::PARAM_STR);
        $statement-> bindValue(':description',$book->description, PDO::PARAM_STR);
        $statement-> execute();
    }

    /** Modifies data related to a book in the collection.
     * @param Book $book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
    */

    public function modifyBook($book)
    {
        $this::verifyBook($book);
        $statement = $this->db-> prepare("UPDATE book SET title = :title, author = :author, description= :description WHERE id = :id");
        $statement-> bindValue(':title', $book->title, PDO::PARAM_STR);
        $statement-> bindValue(':author',$book->author, PDO::PARAM_STR);
        $statement-> bindValue(':description',$book->description, PDO::PARAM_STR);
        $statement->bindValue(':id',$book->id,PDO::PARAM_INT);
        $statement-> execute();
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
    */
    public function deleteBook($id)
    {
        $this::verifyID($id);
        $statement = $this->db->prepare("DELETE FROM book WHERE id = :id");
        $statement->bindValue(':id',$id,PDO::PARAM_INT);
        $statement->execute();
    }

}
