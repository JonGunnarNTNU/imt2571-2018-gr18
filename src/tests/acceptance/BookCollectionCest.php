<?php
use Codeception\Util\Locator;

class BookCollectionCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // Test to verify that the booklist is displayed as expected
    public function showBookListTest(AcceptanceTester $I)
    {
        $I->amOnPage('index.php');
        
        // Book list content
        $I->seeInTitle('Book Collection');
        $I->seeNumberOfElements('table#bookList>tbody>tr', 3);
        // Check sample book values
        $I->see('Jungle Book', 'tr#book1>td:nth-child(2)');
        $I->see('J. Walker', 'tr#book2>td:nth-child(3)');
        $I->see('Written by some smart gal.', 'tr#book3>td:nth-child(4)');
        $I->seeElement('tr#book1>td:first-child>a', ['href' => 'index.php?id=1']);
        $I->seeElement('tr#book2>td:first-child>a', ['href' => 'index.php?id=2']);
        $I->seeElement('tr#book3>td:first-child>a', ['href' => 'index.php?id=3']);
        
        // Add new book form content
        $I->seeElement('form#addForm>input', ['name' => 'title']);
        $I->seeElement('form#addForm>input', ['name' => 'author']);
        $I->seeElement('form#addForm>input', ['name' => 'description']);
        $I->seeElement('form#addForm>input', ['type' => 'submit',
                                              'value' => 'Add new book']);
    }
    
    // Test to verify that the book details page is displayed as expected
    public function showBookDetailsTest(AcceptanceTester $I)
    {
        $I->amOnPage('index.php');
        $I->click(1);
        $this->verifyBookDetails($I, 'Jungle Book', 'R. Kipling', 'A classic book.');
        $I->seeLink('Back to book list','index');
        
        // Buttons for updating and deleting book information
        $I->seeElement('form#modForm>input', ['type' => 'submit',
                                              'value' => 'Update book record']);
        $I->seeElement('form#delForm>input', ['type' => 'submit',
                                              'value' => 'Delete book record']);        
    }
    
    // Test to verify that non-numeric book id's are rejected when requesting book information
    public function invalidBookIdRejectedTest(AcceptanceTester $I)
    {
        $I->amOnPage("index.php?id=1'; drop table book;--");
        $I->seeInTitle('Error Page');        
    }
    
    // Helper function that verifies that the book information on the current page matches the parameter values
    protected function verifyBookDetails(AcceptanceTester $I, String $title, String $author, String $description)
    {
        $I->seeInTitle('Book Details');
        $I->seeElement('form#modForm>input', ['name' => 'title',
                                              'value' => $title]);
        $I->seeElement('form#modForm>input', ['name' => 'author',
                                              'value' => $author]);
        $I->seeElement('form#modForm>input', ['name' => 'description',
                                              'value' => $description]);
    }

    // Test to verify that new books can be added. Four cases should be verified:
    //   1. title=>"New book", author=>"Some author", description=>"Some description"
    //   2. title=>"New book", author=>"Some author", description=>""
    //   3. title=>"A Girl's memoirs", author=>"Jean d'Arc", description=>"Single quotes (') should not break anything"
    //   4. title=>"<script>document.body.style.visibility='hidden'</script>",
    //      author=>"<script>document.body.style.visibility='hidden'</script>",
    //      description=>"<script>document.body.style.visibility='hidden'</script>"
    public function successfulAddBookTest(AcceptanceTester $I)
    {
        $testValues = [['title' => "Different title",
                       'author' => "Different Author",
                       'description' => "Different describtion"],
                       ['title'=>"Different title",'author'=>"Different author",'description'=>""],
                       ['title'=>"A Girl's memoirs", 'author'=>"Jean d'Arc", 'description'=>"Single quotes (') should not break anything"],
                       ['title'=>"<script>document.body.style.visibility='hidden'</script>",'author'=>"<script>document.body.style.visibility='hidden'</script>",'description'=>"<script>document.body.style.visibility='hidden'</script>"]];
        foreach ($testValues as $key) {
            $I->amOnPage('index.php');
            $I->submitForm('#addForm', ['title' => $key['title'], 
                                    'author' => $key['author'],
                                    'description' => $key['description']]);
        }
            $I->seeInTitle('Book Collection');
            $I->seeNumberOfElements('table#bookList>tbody>tr',7);

        $i = 3;
        foreach($testValues as $key) { 
            $i++;
            // Getting booklist with new book added as ID:4
            $I->seeElement("tr#book$i>td:first-child>a", ['href' => "index.php?id=$i"]);
            $I->see($key['title'], "tr#book$i>td:nth-child(2)");
            $I->see($key['author'], "tr#book$i>td:nth-child(3)");
            $I->see($key['description'], "tr#book$i>td:nth-child(4)");
            $I->seeLink("$i","index.php?id=$i");
        }
   }

    // Test to verify that adding a book fails if mandatory fields are missing
    public function addBookWithoutMandatoryFieldsTest(AcceptanceTester $I) 
    {
    //Since both title and author are mandatory I assume that if one of them fails, I can assume that the other would also fail.
      $testValues = ['title' => "",'author'=>"",'description'=>""];
      $I ->amOnPage("index.php");
      $I->submitForm("#addForm",['title'=>$testValues['title'],'author'=>$testValues['author'],'description'=>$testValues['description']]);
      $I ->seeInTitle("Error Page");
    }
    
    // Test to verify that book records can be modified successfully. Four cases should be verified:
    //   1. title=>"Different title", author=>"Different Author", description=>"Different description"
    //   2. title=>"Different title", author=>"Different Author", description=>""
    //   3. title=>"A Girl's memoirs", author=>"Jean d'Arc", description=>"Single quotes (') should not break anything"
    //   4. title=>"<script>document.body.style.visibility='hidden'</script>",
    //      author=>"<script>document.body.style.visibility='hidden'</script>",
    //      description=>"<script>document.body.style.visibility='hidden'</script>"
    public function successfulModifyBookTest(AcceptanceTester $I)
    {
        $testValues = [['title' => "Different title",
                       'author' => "Different Author",
                       'description' => "Different describtion"],
                       ['title'=>"Different title",'author'=>"Different author",'description'=>""],
                       ['title'=>"A Girl's memoirs", 'author'=>"Jean d'Arc", 'description'=>"Single quotes (') should not break anything"],
                       ['title'=>"<script>document.body.style.visibility='hidden'</script>",'author'=>"<script>document.body.style.visibility='hidden'</script>",'description'=>"<script>document.body.style.visibility='hidden'</script>"]];
        foreach ($testValues as $key) {
            $I->amOnPage('index.php?id=2');
            $I->submitForm("#modForm",['title'=>$key['title'],'author'=>$key['author'],'description' => $key['description']]);
            $I->seeInTitle("Book Collection");
            $I->see($key['title'], 'tr#book2>td:nth-child(2)');
            $I->see($key['author'], 'tr#book2>td:nth-child(3)');
            $I->see($key['description'], 'tr#book2>td:nth-child(4)');
        }
    }
    
    // Test to verify that modifying a book fails if mandatory fields are missing
    public function modifyBookWithoutMandatoryFieldsTest(AcceptanceTester $I)
    {
        $I->amOnPage('index.php?id=2');
        $I->submitForm("#modForm",['title'=>'','author'=> 'author']);
        $I->seeInTitle("Error Page"); 

        $I->amOnPage('index.php?id=1');
        $I->submitForm("#modForm",['title'=>'title','author'=> '']);
        $I->seeInTitle("Error Page"); 
    }
    
    // Test to verify that deleting a book succeeds.
    public function successfulDeleteBookTest(AcceptanceTester $I)
    {
        $I->amOnPage('index.php?id=2');
        $I->submitForm("#delForm",[]);
        $I->seeInTitle("Book Collection");
        $I->dontSee("ID: 2");           
    }
    
    // Test to verify that deleting a book succeeds.
    public function deleteBookWithInvalidIdTest(AcceptanceTester $I)
    {
         $I->amOnPage('index.php?id=2');
         $I->submitForm('#delForm',['id'=> 'a']);
         $I->seeInTitle('Error Page');
    }

}