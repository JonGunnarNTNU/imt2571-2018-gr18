<?php

require_once('C:\Users\Leon\Desktop\XAMPP\htdocs\IMT2571\assignment1\Model\DBModel.php');

class BookCollectionTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $dbModel;
    
    protected function _before()
    {
        $db = new PDO(
                'mysql:host=localhost;dbname=test;charset=utf8mb4',
                'root',
                '',
                array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)
            );
        $this->dbModel = new DBModel($db);
    }

    protected function _after()
    {
    }

    // Test that all books are retrieved from the database
    public function testGetBookList()
    {
        $bookList = $this->dbModel->getBookList();

        // Sample tests of book list contents
        $this->assertEquals(3, count($bookList));
        $this->assertEquals(1, $bookList[0]->id);
        $this->assertEquals('Jungle Book', $bookList[0]->title);
        $this->assertEquals(2, $bookList[1]->id);
        $this->assertEquals('J. Walker', $bookList[1]->author);
        $this->assertEquals(3, $bookList[2]->id);
        $this->assertEquals('Written by some smart gal.', $bookList[2]->description);
    }

    // Tests that information about a single book is retrieved from the database
    public function testGetBook()
    {
        $book = $this->dbModel->getBookById(1);

        // Sample tests of book list contents
        $this->assertEquals(1, $book->id);
        $this->assertEquals('Jungle Book', $book->title);
        $this->assertEquals('R. Kipling', $book->author);
        $this->assertEquals('A classic book.',$book->description);
    }

    // Tests that get book operation fails if id is not numeric
    public function testGetBookRejected()
    {
        try{
        $this->dbModel->getBookById("1'; drop table book;--");
        $this->assertInstanceOf(InvalidArgumentException::class,null);
        }catch(InvalidArgumentException $e){};
    }

    // Tests that a book can be successfully added and that the id was assigned. Four cases should be verified:
    //   1. title=>"New book", author=>"Some author", description=>"Some description" 
    //   2. title=>"New book", author=>"Some author", description=>""
    //   3. title=>"<script>document.body.style.visibility='hidden'</script>",
    //      author=>"<script>document.body.style.visibility='hidden'</script>",
    //      description=>"<script>document.body.style.visibility='hidden'</script>"
    public function testAddBook()
    {
        $id = 3;
        $testValues = [['title' => "New book", 'author' => "Some author",'description' => "Some description"], 
                       [ 'title'=>"New book", 'author'=>"Some author", 'description'=>""], 
                       ['title'=>"<script>document.body.style.visibility='hidden'</script>",
                          'author'=>"<script>document.body.style.visibility='hidden'</script>",
                          'description'=>"<script>document.body.style.visibility='hidden'</script>"]];

        foreach ($testValues as $key) {
            $book = new Book($key['title'], $key['author'], $key['description']);
            $this->dbModel->addBook($book);
        }
        // Id was successfully assigned
        $this->tester->seeNumRecords(6, 'book');

        foreach ($testValues as $key) {
            $id++;
      
        // Record was successfully inserted
        $this->tester->seeInDatabase('book', ['id'=>$id,
                                              'title' => $key['title'],
                                              'author' => $key['author'],
                                              'description' => $key['description']]);
        }     
    }

    // Tests that adding a book fails if id is not numeric
    public function testAddBookRejectedOnInvalidId()
    {
        $testValues = ['title'=>'Evil book','author'=>'hackerman','description'=>'ruins your live','id'=>"1'; drop table book;--"];
        $book = new Book($testValues['title'],$testValues['author'],$testValues['description'],$testValues['id']);
        try{
            $this->dbModel->addBook($book);
            $this->assertInstanceOf(InvalidArgumentException::class,null);
        }catch(InvalidArgumentException $e){};
    }

    // Tests that adding a book fails if mandatory fields are left blank
    public function testAddBookRejectedOnMandatoryFieldsMissing()
    {
        $book = new Book("","","");
        try{
        $this->dbModel->addBook($book);
        $this->assertInstanceOf(InvalidArgumentException::class,null);
        }catch(InvalidArgumentException $e){};

    }

    // Tests that a book record can be successfully modified. Three cases should be verified:
    //   1. title=>"New book", author=>"Some author", description=>"Some description"
    //   2. title=>"New book", author=>"Some author", description=>""
    //   3. title=>"<script>document.body.style.visibility='hidden'</script>",
    //      author=>"<script>document.body.style.visibility='hidden'</script>",
    //      description=>"<script>document.body.style.visibility='hidden'</script>"
    public function testModifyBook()
    {
        $id = 0;
        $testValues = [['title' => "New book", 'author' => "Some author",'description' => "Some description"], 
                       [ 'title'=>"New book", 'author'=>"Some author", 'description'=>""], 
                       ['title'=>"<script>document.body.style.visibility='hidden'</script>",
                          'author'=>"<script>document.body.style.visibility='hidden'</script>",
                          'description'=>"<script>document.body.style.visibility='hidden'</script>"]];


        foreach ($testValues as $key) {
            $id++;
            $book = new Book($key['title'], $key['author'], $key['description'], $id);
            $this->dbModel->modifyBook($book);
        }
            // Record was successfully altered
        $id= 0;
        foreach($testValues as $key) {
            $id++;    
            $this->tester->seeInDatabase('book', ['id'=>$id,
                                              'title' => $key['title'],
                                              'author' => $key['author'],
                                              'description' => $key['description']]);
        }     
    } 
    
    // Tests that modifying a book record fails if id is not numeric
    public function testModifyBookRejectedOnInvalidId()
    {
        $testValues = ['title' => "Mean Book", 'author' => "Boogieman", 'description' => "Will mess up everything", 'id' => "delete system32 lol" ];
        $book = new book($testValues['title'],$testValues['author'], $testValues['description'], $testValues['id']);
        try{ 

        $this->dbModel->modifyBook($book);
        $this->assertInstanceOf(InvalidArgumentException::class,null);
        }catch(InvalidArgumentException $e){};
    }
 
    // Tests that modifying a book record fails if mandatory fields are left blank
    public function testModifyBookRejectedOnMandatoryFieldsMissing()
    {  
        $testValues = [['title' => "New Book", 'author' => "", 'description' => "", 'id' => 3],['title'=>"",'author' => " author", 'description' => "", 'id' => 3]];
        foreach ($testValues as $key) {
        $this::modifyBookRejectHelper($key);
        }
    }
    public function modifyBookRejectHelper($testValues) {
        $book = new book($testValues['title'],$testValues['author'], $testValues['description'], $testValues['id']);
        try{ 

        $this->dbModel->modifyBook($book);
        $this->assertInstanceOf(InvalidArgumentException::class,null);
        }catch(InvalidArgumentException $e){};  
    }
    
    // Tests that a book record can be successfully modified.
    public function testDeleteBook()
    {
        $id = 3;
        $this->dbModel->deleteBook($id);
        $this->tester->dontSeeInDatabase('book',['id'=> $id]);

    }
    
    // Tests that adding a book fails if id is not numeric
    public function testDeleteBookRejectedOnInvalidId()
    {
        $id = "Fake ID to get all your money";
        try {
        $this->dbModel->deleteBook($id);
        $this->assertInstanceOf(InvalidArgumentException::class,null);
        }catch(InvalidArgumentException $e){};
    } 
}